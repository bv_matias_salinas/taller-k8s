var express = require('express')
var app = express()

const port = 3000;
const env_nombre = process.env.nombre || "Sin configurar";
const env_apellido = process.env.apellido || "Sin configurar";
const env_secreto_1 = process.env.secreto_1 || "Sin configurar";
const env_secreto_2 = process.env.secreto_2 || "Sin configurar";

app.get('/', function (req, res) {
    res.send({
        nombre: env_nombre,
        apellido: env_apellido,
        secreto_1: env_secreto_1,
        secreto_2: env_secreto_2
    })
})

app.listen(port, () => {
    console.log(`Iniciando aplicacion en http://0.0.0.0:${port}`)
  })
  